// Package cleanmap provides fixes for unmarshaled interface{} maps.
package cleanmap

import (
	"errors"
	"fmt"
)

// CleanMap converts map[interface{}]interface{} to map[string]interface{}.
// It converts all keys of maps and nested maps to string recursively.
func CleanMap(i interface{}) (map[string]interface{}, error) {
	if out, ok := cleanMap(i).(map[string]interface{}); ok {
		return out, nil
	}
	return nil, errors.New("Unable to cast map to map[string]interface{}")
}

func cleanMap(i interface{}) interface{} {
	switch i.(type) {
	case map[interface{}]interface{}:
		return cleanInterfaceMap(i.(map[interface{}]interface{}))
	case map[string]interface{}:
		return cleanStringMap(i.(map[string]interface{}))
	case []interface{}:
		return cleanInterfaceArray(i.([]interface{}))
	case []map[interface{}]interface{}:
		return cleanInterfaceMapArray(i.([]map[interface{}]interface{}))
	case []map[string]interface{}:
		return cleanStringMapArr(i.([]map[string]interface{}))
	default:
		return i
	}
}

func cleanInterfaceMap(m map[interface{}]interface{}) map[string]interface{} {
	res := make(map[string]interface{})
	for k, v := range m {
		key := fmt.Sprintf("%v", k)
		res[key] = cleanMap(v)
	}
	return res
}

func cleanStringMap(m map[string]interface{}) map[string]interface{} {
	for k, v := range m {
		m[k] = cleanMap(v)
	}
	return m
}

func cleanInterfaceArray(arr []interface{}) []interface{} {
	for i, v := range arr {
		arr[i] = cleanMap(v)
	}
	return arr
}

func cleanInterfaceMapArray(mArr []map[interface{}]interface{}) []map[string]interface{} {
	m := make([]map[string]interface{}, len(mArr))
	for i, v := range mArr {
		m[i] = cleanInterfaceMap(v)
	}
	return m
}

func cleanStringMapArr(mArr []map[string]interface{}) []map[string]interface{} {
	for i, v := range mArr {
		mArr[i] = cleanStringMap(v)
	}
	return mArr
}
